#!/usr/bin/python3

from operator import itemgetter


class Kruskal:


	def __init__(self):
		self.tree = []



	def new(self, departure, arrived, value):

		"""Adds a new edge to the tree / graph."""

		self.tree.append((departure, arrived, value))



	def Kruskal(self):

		"""Search for minimum weight overlay tree (ARPM) or minimum overlay tree (ACM) in a related unoriented and weighted graph."""

		def find(e, node, node2):

			r = {
				node: None,
				node2: None
			}

			for v in enumerate(e):

				if node in v[1]:
					r[node] = v[0]

				if node2 in v[1]:
					r[node2] = v[0]

			return r


		mst = [] # MST : Minimum Spanning Tree
		tmp = []

		for v in sorted(self.tree, key=itemgetter(2)):

			r = find(tmp, v[0], v[1])

			if r[v[0]] is None:
				if r[v[1]] is not None:
					tmp[r[v[1]]].append(v[0])
					mst.append(v)


			if r[v[1]] is None:
				if r[v[0]] is not None:
					tmp[r[v[0]]].append(v[1])
					mst.append(v)


			if r[v[0]] is None:
				if r[v[1]] is None:
					tmp.append([v[0], v[1]])
					mst.append(v)
					

			if r[v[0]] is not None:
				if r[v[1]] is not None:

					# Avoids forming a cycle
					if r[v[0]] != r[v[1]]:

						if r[v[0]] >= r[v[1]]:
							tmp[r[v[0]]].extend(tmp[r[v[1]]])

						if r[v[1]] > r[v[0]]:
							tmp[r[v[1]]].extend(tmp[r[v[0]]])

						mst.append(v)	

		tmp = None

		return mst



	def show(self):

		"""Displays the content of the tree / graph."""

		[print(v) for v in self.tree]