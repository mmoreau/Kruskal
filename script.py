#!/usr/bin/python3

from Kruskal import Kruskal

obj = Kruskal()

obj.new(0, 1, 4)
obj.new(0, 7, 8)
obj.new(1, 7, 11)
obj.new(1, 2, 8)
obj.new(2, 8, 2)
obj.new(2, 5, 4)
obj.new(2, 3, 7)
obj.new(3, 4, 9)
obj.new(3, 5, 14)
obj.new(4, 5, 10)
obj.new(5, 6, 2)
obj.new(6, 8, 6)
obj.new(6, 7, 1)
obj.new(7, 8, 7)

[print(v) for v in obj.Kruskal()]